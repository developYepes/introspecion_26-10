package com.introspeccion;

public class UsoEmpleado {

	public static void main(String[] args) {
		Persona antonio = new Persona("Antonio");
		System.out.println(antonio.getNombre());
		
		Empleado ana = new Empleado("Ana", 2000);
		System.out.println(ana.getNombre());
		System.out.println(ana.getSalario());
		
//		Class clase = antonio.getClass();
//		System.out.println(clase.getName());
		String nombreClase = "com.introspeccion.Persona";
		try {
			Class clase = Class.forName(nombreClase);
			System.out.println(clase.getName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(Empleado.class.getName());
		
	}

}

class Persona{
	private String nombre;

	public Persona(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}
	
	
}
class Empleado extends Persona {
	private double salario;

	public Empleado(String nombre, double salario) {
		super(nombre);
		this.salario = salario;
	}
	void setIncentivo(double incentivo) {
		salario = salario + incentivo;
	}
	public String getSalario() {
		return "El salario es: " + salario;
	}
}
