package com.introspeccion;

import java.lang.reflect.*;
import java.util.Scanner;

public class PruebaIntrospeccion {
	public static void main(String[] args) {
		String nombreClase;

		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduce el nombre de la clase");

		nombreClase = entrada.next();

		// Imprimir clase y superclase
		try {
			Class clase = Class.forName(nombreClase);
			Class superClase = clase.getSuperclass();

			System.out.println("Clase: " + nombreClase);

			if (superClase != null && superClase != Object.class) {
				System.out.println("Extends -> " + superClase.getName());
			}
			System.out.println("***** CONSTRUCTORES *******");
			imprimirConstructores(clase);

			System.out.println("\n***** METODOS *********");
			imprimirMetodos(clase);

			System.out.println("******* CAMPOS *********");
			imprimirCampos(clase);

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);

		}

	}

	private static void imprimirCampos(Class clase) {
		Field[] campos = clase.getDeclaredFields();
		for(Field campo: campos) {
			Class tipoCampo = campo.getType();
			String nombre = campo.getName();
			System.out.print("    " + Modifier.toString(campo.getModifiers()));
			System.out.print("  " + tipoCampo.getName() + " " + nombre + "(");
		}
	}

	private static void imprimirMetodos(Class clase) {
		Method[] metodos = clase.getDeclaredMethods();
		for (Method metodo : metodos) {
			Class tipoDevuelto = metodo.getReturnType();
			String nombre = metodo.getName();

			// Imprime modificadores, tipo y nombre

			System.out.print("    " + Modifier.toString(metodo.getModifiers()));
			System.out.print("  " + tipoDevuelto.getName() + " " + nombre + "(");

			// Imprime tipo de parametros

			Class[] tipoParams = metodo.getParameterTypes();
			for (int i = 0; i < tipoParams.length; i++) {
				if (i > 0)
					System.out.print(",   ");

				System.out.print(tipoParams[i].getName());
			}
			System.out.println(");");
		}

	}

	private static void imprimirConstructores(Class clase) {
		Constructor[] constructores = clase.getDeclaredConstructors();

		for (Constructor c : constructores) {
			String nombre = c.getName();
			System.out.println("   " + Modifier.toString(c.getModifiers()));
			System.out.print("  " + nombre + "(");

			// IMPRIMIR PARAMETROS
			Class[] parametros = c.getParameterTypes();
			for (int i = 0; i < parametros.length; i++) {
				if (i > 0)
					System.out.print(",   ");
				System.out.print(parametros[i].getName());
			}
			System.out.println(");");
		}

	}

}
